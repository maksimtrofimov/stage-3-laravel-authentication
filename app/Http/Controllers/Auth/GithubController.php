<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Socialite;
use App\Providers\RouteServiceProvider;
use App\Entity\User;
use Illuminate\Support\Facades\Hash;
use Auth;

class GithubController extends Controller
{
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('github')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        $userSocial = Socialite::driver('github')->stateless()->user();

        $user = User::where('email', $userSocial->email)->first();
        if ($user === null) {
            $user = new User();
            $user->name = $userSocial->getNickname();
            $user->email = $userSocial->getEmail();
            $user->password = Hash::make('qweasd');
            $user->save();
        }

        Auth::login($user);

        return redirect($this->redirectTo);
    }
}
